package com.example.gexuanyang.myapplication;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.view.*;

import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by gexuanyang on 2015/2/21.
 */


public class subjectAdapter extends ArrayAdapter<searchsource.myClass> {
    public subjectAdapter(Context context, ArrayList<searchsource.myClass> users) {
       super(context, 0, users);
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        searchsource.myClass user = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_user, parent, false);
        }
        // Lookup view for data population
        ImageView iv=(ImageView)convertView.findViewById(R.id.icon);
        try{
            URL url = new URL(user.url);
            InputStream content = (InputStream)url.getContent();
            Drawable d = Drawable.createFromStream(content , "src");
            iv.setImageDrawable(d);
            TextView tvName = (TextView) convertView.findViewById(R.id.tvName);
            TextView address = (TextView) convertView.findViewById(R.id.tvAddress);
            // Populate the data into the template view using the data object
            tvName.setText("Name: "+user.name);
            address.setText("Address: "+user.address);
            // Return the completed view to render on screen
            convertView.setId(user.i);
            return convertView;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
