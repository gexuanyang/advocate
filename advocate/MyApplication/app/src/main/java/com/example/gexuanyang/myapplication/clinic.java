package com.example.gexuanyang.myapplication;

/**
 * Created by gexuanyang on 2015/2/21.
 */
public class clinic {

    //uniqueID;
    private String uniqueID;
    void setID(String name) {
        uniqueID=name;
    }
    String getID() {
        return uniqueID;
    }

    //clinicResourceName
    private String clinicResourceName;
    void setResourceName(String name) {
        clinicResourceName=name;
    }
    String getResourceName() {
        return clinicResourceName;
    }

    //serviceType
    private String serviceType;
    void setServiceType(String type) {
        serviceType=type;
    }
    String getServiceType() {
        return serviceType;
    }

    //serviceTypeOtherInfo
    private String serviceOtherInfo="";
    void setServiceOtherInfo(String info) {
        serviceOtherInfo=info;
    }
    String getServiceOtherInfo() {
        return serviceOtherInfo;
    }

    //serviceProvided
    private String serviceProvided="";
    void setServiceProvided(String s) {
        serviceProvided=s;
    }
    String getServiceProvided() {
        return serviceProvided;
    }

    //address
    private String address="",zip="",city="",state="";
    void setAddress(String s) {
        address=s;
    }
    String getAddress() {
        return address;
    }
    void setZip(String s) {
        zip=s;
    }
    String getZip() {
        return zip;
    }
    void setCity(String s) {
        city=s;
    }
    String getCity() {
        return city;
    }
    void setState(String s) {
        state=s;
    }
    String getState() {
        return state;
    }

    //map
    private String mapLink="", busRoute="";
    private boolean siteOnBusRoute=false;
    void setMapLink(String s) {
        mapLink=s;
    }
    String getMaplink() {
        return mapLink;
    }
    void setBusRoute(String s) {
        busRoute=s;
    }
    String getBusRoute() {
        return busRoute;
    }
    void setSiteOnBusRoute(boolean s) {
        siteOnBusRoute=s;
    }
    boolean getSiteOnBusRoute() {
        return siteOnBusRoute;
    }

    //phoneNum1
    private String phoneNum1="",phoneInfo1="";
    private boolean phoneVer1=false;
    void setPhoneNum1(String s) {
        phoneNum1=s;
    }
    String getPhoneNum1() {
        return phoneNum1;
    }
    void setPhoneInfo1(String s) {
        phoneInfo1=s;
    }
    String getPhoneInfo1() {
        return phoneInfo1;
    }
    void setPhoneVer1(boolean s) {
        phoneVer1=s;
    }
    boolean getPhoneVer1() {
        return phoneVer1;
    }

    //phoneNum2
    private String phoneNum2="",phoneInfo2="";
    private boolean phoneVer2=false;
    void setPhoneNum2(String s) {
        phoneNum2=s;
    }
    String getPhoneNum2() {
        return phoneNum2;
    }
    void setPhoneInfo2(String s) {
        phoneInfo2=s;
    }
    String getPhoneInfo2() {
        return phoneInfo2;
    }
    void setPhoneVer2(boolean s) {
        phoneVer2=s;
    }
    boolean getPhoneVer2() {
        return phoneVer2;
    }

    //online
    private String website="",email="",otherContactInfo="";
    void setWeb(String s) {
        website=s;
    };
    String getWeb() {
        return website;
    }
    void setEmail(String s) {
        email=s;
    }
    String getEmail() {
        return email;
    }
    void setOtherContactInfo(String s) {
        otherContactInfo=s;
    }
    String getOtherContactInfo() {
        return otherContactInfo;
    }

    //time
    private String[] timeCheck=new String[7];
    private String[] timeOpened=new String[7],timeClosed=new String[7];
    private String otherTimeInfo="";
    void setTimeCheck(int day, String t) {
        timeCheck[day]=t;
    }
    String getTimeCheck(int day) {
        return timeCheck[day];
    }
    void setTimeOpened(int day,String s)
    {
        timeOpened[day]=s;
    }
    String getTimeOpened(int day) {
        return timeOpened[day];
    }
    void setTimeClosed(int day,String s)
    {
        timeClosed[day]=s;
    }
    String getTimeClosed(int day) {
        return timeClosed[day];
    }
    void setOtherTimeInfo(String s) {
        otherTimeInfo=s;
    }
    String getOtherTimeInfo() {
        return otherTimeInfo;
    }

    //eligibility
    private String criteriaAll="",criteriaUninsured="",criteriaIncomeDocumentation="",criteriaTennCare="",criteriaPrivateInsurance="";
    private String otherEligibilityCriteriaInfo="";
    void setCriteriaAll(String s) {
        criteriaAll=s;
    }
    String getCriteriaAll() {
        return criteriaAll;
    }
    void setCriteriaUninsured(String s) {
        criteriaUninsured=s;
    }
    String getCriteriaUninsured() {
        return criteriaUninsured;
    }
    void setCriteriaIncomeDocumentation(String s) {
        criteriaIncomeDocumentation=s;
    }
    String getCriteriaIncomeDocumentation() {
        return criteriaIncomeDocumentation;
    }
    void setCriteriaTennCare(String s) {
        criteriaTennCare=s;
    }
    String getCriteriaTennCare() {
        return criteriaTennCare;
    }
    void setOtherEligibilityCriteriaInfo(String s) {
        otherEligibilityCriteriaInfo=s;
    }
    String getOtherEligibilityCriteriaInfo() {
        return otherEligibilityCriteriaInfo;
    }
    void setCriteriaPrivateInsurance(String s) {
        criteriaPrivateInsurance=s;
    }
    String getCriteriaPrivateInsurance() {
        return criteriaPrivateInsurance;
    }

    //fee
    private String feesSlidingScale="",feesMinimumPayment="",feesOtherInfo="";
    void setFeesSlidingScale(String s) {
        feesSlidingScale=s;
    }
    String getFeesSlidingScale() {
        return feesSlidingScale;
    }
    void setFeesMinimumPayment(String s) {
        feesMinimumPayment=s;
    }
    String getFeesMinimumPayment() {
        return feesMinimumPayment;
    }
    void setFeesOtherInfo(String s) {
        feesOtherInfo=s;
    }
    String getFeesOtherInfo() {
        return feesOtherInfo;
    }

    //language
    private String languageEnglish="",languageSpanish="",languageOther="",languageOtherInfo="";
    void setLanguageEnglish(String s) {
        languageEnglish=s;
    }
    String getLanguageEnglish() {
        return languageEnglish;
    }
    void setLanguageSpanish(String s) {
        languageSpanish=s;
    }
    String getLanguageSpanish() {
        return languageSpanish;
    }
    void setLanguageOther(String s) {
        languageOther=s;
    }
    String getLanguageOther() {
        return languageOther;
    }
    void setLanguageOtherInfo(String s) {
        languageOtherInfo=s;
    }
    String getLanguageOtherInfo() {
        return languageOtherInfo;
    }

    //other tag
    private String otherTag;
    void setOtherTag(String s) {
        otherTag=s;
    }
    String getOtherTag() {
        return otherTag;
    }

    //other note
    private String otherNote="";
    void setOtherNote(String s) {
        otherNote=s;
    }
    String getOtherNote() {
        return otherNote;
    }

    //img URL;
    private String img="";
    void setIMG(String s) {
        img=s;
    }
    String getIMG() {
        return img;
    }
}
