package com.example.gexuanyang.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

/**
 * Created by gexuanyang on 2015/2/21.
 */
public class displayHospital extends Activity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Always call super class for necessary
        // initialization/implementation.
        super.onCreate(savedInstanceState);
        final String myString[]=getIntent().getStringArrayExtra("Hospital information");
        ListView lv= new ListView(getApplicationContext());
        ArrayAdapter<String> myAdapter =new ArrayAdapter<String>(this, R.id.listView,myString);
        lv.setAdapter(myAdapter);
        setContentView(R.layout.displayhospital);
        final Button search = (Button) findViewById(R.id.mySearch);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent geoIntent = makeGeoIntent(myString[1]+myString[2]);

                // Check to see if there's a Map app to handle the "geo" intent.
                if (geoIntent.resolveActivity(getPackageManager()) != null)
                    startActivity(geoIntent);
                else
                    // Start the Browser app instead.
                    startActivity(makeMapsIntent(myString[1]+myString[2]));
            }
        });
    }
    private Intent makeGeoIntent(String address) {
        return new Intent(Intent.ACTION_VIEW,
                Uri.parse("geo:0,0?q="
                        + address));
    }
    private Intent makeMapsIntent(String address) {
        return new Intent(Intent.ACTION_VIEW,
                Uri.parse("http://maps.google.com/?q="
                        + address));
    }
}
