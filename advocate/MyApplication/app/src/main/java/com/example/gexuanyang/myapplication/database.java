package com.example.gexuanyang.myapplication;
import java.io.*;

import com.opencsv.CSVReader;

/**
 * Created by gexuanyang on 2015/2/21.
 */
public class database {

    private clinic[] clinics = new clinic[200];
    private int num=0;

    void initialization() throws Exception {
        CSVReader reader = null;
        try {
            reader = new CSVReader(new FileReader("parseData_AdvoCodeDataSet.csv"),',');
            String[] nextLine;
            clinic[] clinics = new clinic[200];
            while ((nextLine=reader.readNext())!=null) {
                //num++;
                clinics[num]=new clinic();
                clinics[num].setID(nextLine[0]);
                clinics[num].setResourceName(nextLine[1]);
                clinics[num].setServiceType(nextLine[2]);
                clinics[num].setServiceOtherInfo(nextLine[3]);
                clinics[num].setAddress(nextLine[4]);
                clinics[num].setCity(nextLine[5]);
                clinics[num].setState(nextLine[6]);
                clinics[num].setZip(nextLine[7]);
                clinics[num].setMapLink(nextLine[8]);
                if ((nextLine[9]=="Yes")||(nextLine[9]=="yes")) clinics[num].setSiteOnBusRoute(true); else clinics[num].setSiteOnBusRoute(false);
                clinics[num].setBusRoute(nextLine[10]);
                clinics[num].setPhoneNum1(nextLine[11]);
                clinics[num].setPhoneInfo1(nextLine[12]);
                if ((nextLine[13]=="Yes")||(nextLine[13]=="yes")) clinics[num].setPhoneVer1(true); else clinics[num].setPhoneVer1(false);
                clinics[num].setPhoneNum2(nextLine[14]);
                clinics[num].setPhoneInfo2(nextLine[15]);
                if ((nextLine[16]=="Yes")||(nextLine[16]=="yes")) clinics[num].setPhoneVer2(true); else clinics[num].setPhoneVer2(false);
                clinics[num].setWeb(nextLine[17]);
                clinics[num].setEmail(nextLine[18]);
                clinics[num].setOtherContactInfo(nextLine[19]);
                clinics[num].setTimeCheck(0,nextLine[20]);
                clinics[num].setTimeOpened(0,nextLine[21]);
                clinics[num].setTimeClosed(0,nextLine[22]);
                clinics[num].setTimeCheck(1,nextLine[23]);
                clinics[num].setTimeOpened(1,nextLine[24]);
                clinics[num].setTimeClosed(1,nextLine[25]);
                clinics[num].setTimeCheck(2,nextLine[26]);
                clinics[num].setTimeOpened(2,nextLine[27]);
                clinics[num].setTimeClosed(2,nextLine[28]);
                clinics[num].setTimeCheck(3,nextLine[29]);
                clinics[num].setTimeOpened(3,nextLine[30]);
                clinics[num].setTimeClosed(3,nextLine[31]);
                clinics[num].setTimeCheck(4,nextLine[32]);
                clinics[num].setTimeOpened(4,nextLine[33]);
                clinics[num].setTimeClosed(4,nextLine[34]);
                clinics[num].setTimeCheck(5,nextLine[35]);
                clinics[num].setTimeOpened(5,nextLine[36]);
                clinics[num].setTimeClosed(5,nextLine[37]);
                clinics[num].setTimeCheck(6,nextLine[38]);
                clinics[num].setTimeOpened(6,nextLine[39]);
                clinics[num].setTimeClosed(6,nextLine[40]);
                clinics[num].setOtherTimeInfo(nextLine[41]);
                clinics[num].setServiceProvided(nextLine[42]);
                clinics[num].setCriteriaAll(nextLine[43]);
                clinics[num].setCriteriaUninsured(nextLine[44]);
                clinics[num].setCriteriaIncomeDocumentation(nextLine[45]);
                clinics[num].setCriteriaTennCare(nextLine[46]);
                clinics[num].setCriteriaPrivateInsurance(nextLine[47]);
                clinics[num].setOtherEligibilityCriteriaInfo(nextLine[48]);
                clinics[num].setFeesSlidingScale(nextLine[49]);
                clinics[num].setFeesMinimumPayment(nextLine[50]);
                clinics[num].setFeesOtherInfo(nextLine[51]);
                clinics[num].setLanguageEnglish(nextLine[52]);
                clinics[num].setLanguageSpanish(nextLine[53]);
                clinics[num].setLanguageOther(nextLine[54]);
                clinics[num].setLanguageOtherInfo(nextLine[55]);
                clinics[num].setOtherTag(nextLine[56]);
                clinics[num].setOtherNote(nextLine[57]);
                clinics[num].setIMG(nextLine[58]);
                //nextLine=reader.readNext();
                num++;
            }
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            try {
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    int getNum() {
        return num;
    }

    String getID(int i) {
        return clinics[i].getID();
    }

    String getResourceName(int i) {
        return clinics[i].getResourceName();
    }

    String getServiceType(int i) {
        return clinics[i].getServiceType();
    }

    String getServiceOtherInfo(int i) {
        return clinics[i].getServiceOtherInfo();
    }

    String getServiceProvided(int i) {
        return clinics[i].getServiceProvided();
    }

    String getAddress(int i) {
        return clinics[i].getAddress();
    }

    String getZip(int i) {
        return clinics[i].getZip();
    }

    String getCity(int i) {
        return clinics[i].getCity();
    }

    String getState(int i) {
        return clinics[i].getState();
    }

    String getMapLink(int i) {
        return clinics[i].getMaplink();
    }

    String getBusRoute(int i) {
        return clinics[i].getBusRoute();
    }

    boolean getSiteOnBusRoute(int i) {
        return clinics[i].getSiteOnBusRoute();
    }

    String getPhoneNum1(int i) {
        return clinics[i].getPhoneNum1();
    }

    String getPhoneInfo1(int i) {
        return clinics[i].getPhoneInfo1();
    }

    boolean getPhoneVer1(int i) {
        return clinics[i].getPhoneVer2();
    }

    String getPhoneNum2(int i) {
        return clinics[i].getPhoneNum2();
    }

    String getPhoneInfo2(int i) {
        return clinics[i].getPhoneInfo2();
    }

    boolean getPhoneVer2(int i) {
        return clinics[i].getPhoneVer2();
    }

    String getWeb(int i) {
        return clinics[i].getWeb();
    }

    String getEmail(int i) {
        return clinics[i].getEmail();
    }

    String getOtherContactInfo(int i) {
        return clinics[i].getOtherContactInfo();
    }

    String getTimeCheck(int i,int day) {
        return clinics[i].getTimeCheck(day);
    }

    String getTimeOpened(int i,int day) {
        return clinics[i].getTimeOpened(day);
    }

    String getTimeClosed(int i,int day) {
        return clinics[i].getTimeClosed(day);
    }

    String getOtherTimeInfo(int i) {
        return clinics[i].getOtherTimeInfo();
    }

    String getCriteriaAll(int i) {
        return clinics[i].getCriteriaAll();
    }

    String getCriteriaUninsured(int i) {
        return clinics[i].getCriteriaUninsured();
    }

    String getCriteriaIncomeDocumentation(int i) {
        return clinics[i].getCriteriaIncomeDocumentation();
    }

    String getCriteriaTennCare(int i) {
        return clinics[i].getCriteriaTennCare();
    }

    String getOtherEligibilityCriteriaInfo(int i) {
        return clinics[i].getOtherEligibilityCriteriaInfo();
    }

    String getCriteriaPrivateInsurance(int i) {
        return clinics[i].getCriteriaPrivateInsurance();
    }

    String getFeesSlidingScale(int i) {
        return clinics[i].getFeesSlidingScale();
    }

    String getFeesMinimumPayment(int i) {
        return clinics[i].getFeesMinimumPayment();
    }

    String getFeesOtherInfo(int i) {
        return clinics[i].getFeesOtherInfo();
    }

    String getLanguageEnglish(int i) {
        return clinics[i].getLanguageEnglish();
    }

    String getLanguageSpanish(int i) {
        return clinics[i].getLanguageSpanish();
    }

    String getLanguageOther(int i) {
        return clinics[i].getLanguageOther();
    }

    String getLanguageOtherInfo(int i) {
        return clinics[i].getLanguageOtherInfo();
    }

    String getOtherTag(int i) {
        return clinics[i].getOtherTag();
    }

    String getOtherNote(int i) {
        return clinics[i].getOtherNote();
    }

    String getIMG(int i) {
        return clinics[i].getIMG();
    }

    //search
    private boolean searchClinic(String rhs,String keyWord) {
        boolean flag=false;
        for (int i=0;i<rhs.length()-keyWord.length()+1;i++) {
            if (rhs.charAt(i)==keyWord.charAt(0)) {
                flag=true;
                for (int j=1;j<keyWord.length();j++) {
                    if (rhs.charAt(i+j)!=keyWord.charAt(j)) {
                        flag=false;
                        break;
                    }
                }
                if (flag) return flag;
            }
        }
        return false;
    }
    public boolean[] search(String keyWord){
        boolean[] answer=new boolean[200];
        boolean flag;
        for (int i=0;i<num;i++) {
            flag=searchClinic(clinics[i].getResourceName(),keyWord)||searchClinic(clinics[i].getServiceType(),keyWord)||searchClinic(clinics[i].getServiceOtherInfo(),keyWord);
            flag=flag||searchClinic(clinics[i].getAddress(),keyWord)||searchClinic(clinics[i].getServiceProvided(),keyWord)||searchClinic(clinics[i].getOtherTag(),keyWord)||searchClinic(clinics[i].getOtherNote(),keyWord);
            if (flag) {
                answer[i]=true;
            } else answer[i]=false;
        }
        return answer;

    }

    //filter
    public boolean[] filter(boolean[] b) {
        boolean[] answer=new boolean[200];
        for (int i=0;i<num;i++) {
            answer[i]=false;
            if (b[0])
                if (!clinics[i].getSiteOnBusRoute())  continue;

            //b[1]==true if it has medical(or Medical)
            if (b[1])
                if ((searchClinic(clinics[i].getServiceType(),"Medical")||(searchClinic(clinics[i].getServiceType(),"medical")!=b[1]))) continue;

            if (b[2])
                if ((clinics[i].getCriteriaAll()=="Checked")!=b[2]) continue;

            if (b[3])
                if ((clinics[i].getCriteriaUninsured()=="Checked")!=b[3]) continue;

            if (b[4])
                if ((clinics[i].getCriteriaIncomeDocumentation()=="Checked")!=b[4]) continue;

            if (b[5])
                if ((clinics[i].getCriteriaTennCare()=="Checked")!=b[5]) continue;

            if (b[6])
                if ((clinics[i].getCriteriaPrivateInsurance()=="Checked")!=b[6]) continue;

            if (b[7])
                if ((clinics[i].getFeesSlidingScale()=="Checked")!=b[7]) continue;

            if (b[8])
                if ((clinics[i].getFeesMinimumPayment()=="Checked")!=b[8]) continue;

            if (b[9])
                if ((clinics[i].getLanguageEnglish()=="Checked")!=b[9]) continue;

            if (b[10])
                if ((clinics[i].getLanguageSpanish()=="Checked")!=b[10]) continue;

            if (b[11])
                if ((clinics[i].getLanguageOther()=="Checked")!=b[11]) continue;

            answer[i]=true;
        }
        return answer;
    }
}
