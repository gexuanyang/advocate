package com.example.gexuanyang.myapplication;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.io.*;
import java.util.ArrayList;

import com.opencsv.CSVReader;

/**
 * Created by gexuanyang on 2015/2/21.
 */
public class searchsource extends ListActivity {
    /**
     * Debugging tag used by the Android logger.
     */
    private final String TAG = getClass().getSimpleName();
    private static final int filter=1;
    private database myData;
    private EditText mUrlEditText;
    private String type;
    private boolean[] restraint=new boolean[12];
    private final boolean[] refresh={false,false,false,false,false,false,false,false,false,false,false,false};
    //private boolean preference;
    public class myClass{
        String name;
        String address;
        String url;
        int i;
        myClass(String name1, String address1, String url1,int i1){
            name=name1;
            address=address1;
            url=url1;
            i=i1;
        }
    }

    public void onCreate(final Bundle savedInstanceState) {
        try{
            super.onCreate(savedInstanceState);
            mUrlEditText=(EditText)findViewById(R.id.searchText);
            setContentView(R.layout.serachsource);
            //Restraint is for filter
            final Runnable myRun
                    = new Runnable(){
                public void run(){
                    restraint=getIntent().getBooleanArrayExtra("filter");
                    myData=new database();
                    String searchWord;
                    if(mUrlEditText.toString()=="searchText"){
                        searchWord=getIntent().getData().toString();
                    }else{
                        searchWord=getUrl().toString();
                    }
                    boolean[] searchAnswer=myData.search(searchWord);
                    boolean[] filterAnswer=myData.filter(restraint);
                    boolean[] finalAnswer=new boolean[myData.getNum()];
                    final myClass[] myList=new myClass[myData.getNum()];
                    int tot=0;
                    for (int i=0;i<myData.getNum();i++)
                        if (searchAnswer[i]&&filterAnswer[i]) {
                            finalAnswer[i]=true;
                            myList[tot]= new myClass(myData.getResourceName(i),myData.getAddress(i)+myData.getZip(i),myData.getIMG(i),i);
                            tot++;
                        } else finalAnswer[i]=false;
                    runOnUiThread(new Runnable(){
                                      public void run(){
                                          ArrayList<myClass> arrayOfUsers = new ArrayList<myClass>();
                                          subjectAdapter adapter = new subjectAdapter(searchsource.this, arrayOfUsers);
                                          ListView lv = (ListView) findViewById(R.id.listView);
                                          lv.setAdapter(adapter);
                                          setContentView(R.layout.serachsource);
                                          lv.setOnClickListener(new View.OnClickListener() {
                                              @Override
                                              public void onClick(View v) {
                                                  Intent intent=new Intent();
                                                  String [] myString =new String[18];
                                                  int index=v.getId();
                                                  myString[0]="Name: " +myData.getResourceName(index);
                                                  myString[1]="Address: "+ myData.getAddress(index)+" "+myData.getCity(index)+" "+myData.getState(index);
                                                  myString[2]=myData.getZip(index);

                                                  myString[3]="Service type: "+ myData.getServiceType(index);
                                                  myString[4]="Service other info: "+myData.getServiceOtherInfo(index);
                                                  myString[5]="Service provided: "+ myData.getServiceProvided(index);

                                                  String[] day={"Mon","Tue","Wed","Thur","Fri","Sat","Sun"};

                                                  for (int i=6;i<13;i++) {
                                                      myString[i]=day[i-6]+":"+myData.getTimeCheck(index,i-6);
                                                      if (myData.getTimeCheck(index,i-6)=="Open") {
                                                          myString[i]=myString[i]+myData.getTimeOpened(index,i-6)+"~"+myData.getTimeClosed(index,i-6);
                                                      }
                                                  }

                                                  myString[14]="Web Address: "+ myData.getWeb(index);
                                                  myString[15]="Email: "+myData.getEmail(index);
                                                  myString[16]="Phone Number: " + myData.getPhoneNum1(index)+" "+myData.getPhoneInfo1(index);
                                                  myString[17]="Phone Number2: "+ myData.getPhoneNum2(index)+" "+myData.getPhoneInfo2(index);
                                                  myString[18]="Otehr Contact information: "+myData.getOtherContactInfo(index);

                                                  intent.putExtra("Hospital information", myString);
                                                  intent.setClass(searchsource.this, displayHospital.class);
                                                  startActivity(intent);
                                              }
                                          });
                                      }
                                      });
                    finish();
                }
            };
            new Thread(myRun).start();
            final Button search = (Button) findViewById(R.id.mySearch);
            search.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new Thread(myRun).start();
                }
            });
            //havent finished
            final Button filt = (Button) findViewById(R.id.filter);
            filt.setOnClickListener(new View.OnClickListener() {
                   public void onClick(View v){
                       myDialog choose=new myDialog();
                       choose.show("","");
                       new Thread(myRun).start();
                   }

            });
            //havent finished here
            final Button feedback = (Button) findViewById(R.id.feedback);
            feedback.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v){
                    Intent buttonIntent= new Intent();
                    buttonIntent.setClass(searchsource.this,feedbackActivity.class);
                    startActivity(feedbackActivity);
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //set preference
    //public void onSaveInstanceState(Bundle savedInstanceState){
    //    super.onSaveInstanceState(savedInstanceState);
    //    savedInstanceState.putBoolean("preference",);
    //}


    private class myDialog extends android.support.v4.app.DialogFragment{
        @Override
        public Dialog onCreateDialog (Bundle savedInstanceState) {
            final boolean[] mSelectedItems ={false,false,false,false,false,false,false,false,false,false,false,false};
            // Where we track the selected items
            AlertDialog.Builder builder = new AlertDialog.Builder(searchsource.this);
            // Set the dialog title
            builder.setTitle("Filter")
                    // Specify the list array, the items to be selected by default (null for none),
                    // and the listener through which to receive callbacks when items are selected
                    .setMultiChoiceItems(R.array.Filter, null,
                            new DialogInterface.OnMultiChoiceClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which,
                                                    boolean isChecked) {
                                    if (isChecked) {
                                        // If the user checked the item, add it to the selected items
                                        mSelectedItems[Integer.valueOf(which)] = true;
                                    } else if (mSelectedItems[Integer.valueOf(which)] == true) {
                                        // Else, if the item is already in the array, remove it
                                        mSelectedItems[Integer.valueOf(which)] = false;
                                    }
                                }
                            })
                            // Set the action buttons
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            restraint = mSelectedItems;
                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            cancel();
                        }
                    });
            return builder.create();
        }
        public void cancel(){
            onDestroy();
        }
        public void dismiss(){
            onDestroy();
        }
    }


    protected Uri getUrl() {
        Uri url = null;

        // Get the text the user typed in the edit text (if anything).
        url = Uri.parse(mUrlEditText.getText().toString());

        // If the user didn't provide a URL then use the default.
        String uri = url.toString();
        return url;
    }


}
